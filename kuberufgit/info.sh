#!/bin/bash
CLUSTER=$(kubectl config view --minify | grep name | cut -f 2- -d ":" | tr -d " " | head -n 1)
APISERVER=$(kubectl config view --minify | grep server | cut -f 2- -d ":" | tr -d " ")
CAPATH=$(echo "${KUBECONFIG%/*}");CAFILE=$(kubectl config view --minify | grep certificate-authority | cut -f 2- -d ":" | tr -d " ")
TOKEN=$(kubectl describe secret -n gitlab-ufam $(kubectl get secrets -n gitlab-ufam | grep ^default | cut -f1 -d ' ') | grep -E '^token' | cut -f2 -d':' | tr -d " ")

echo "Cluster name: "
echo $CLUSTER
echo ""
echo "API URL: " 
echo $APISERVER
echo ""
echo "Token: "
echo $TOKEN
echo ""
echo "CA: "
cat $CAPATH/$CAFILE
echo ""
echo "Namespace:"
echo "gitlab-managed-apps"
