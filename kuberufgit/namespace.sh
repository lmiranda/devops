#!/bin/bash
./kubectl create -f - <<EOF
apiVersion: v1
kind: Namespace
metadata:
  name: gitlab-ufam
spec:
  finalizers:
  - kubernetes
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-ci
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: User
  name: system:serviceaccount:gitlab-ufam:default
---
EOF
