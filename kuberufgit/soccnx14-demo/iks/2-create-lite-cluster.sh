#!/bin/bash
# Maintainer: Nico Meisenzahl <nico@meisenzahl.org>
# Not intended for production!

# Customize based on your environment

# Disable colors
export BLUEMIX_COLOR=false
export K8S_VERSION=1.11.2

# Change Region (Lite Cluster isn't available in all regions)
ibmcloud ks region-set eu-gb

# Create new IKS Lite CLuster
# More details: https://console.bluemix.net/docs/containers/cs_clusters.html#clusters
ibmcloud ks cluster-create --name soccnx_cluster --kube-version $K8S_VERSION
ibmcloud ks clusters

# Display worker status
watch -t ibmcloud ks workers soccnx_cluster
